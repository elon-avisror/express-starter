import * as express from 'express';
import { logger, weblogger } from './lib/Logger';
import * as bodyParser from 'body-parser';

export default class App {
    public express: express.Application;

    public constructor() {
        logger.info('Starting App');

        this.express = express();

        this.express.use(weblogger);
        this.express.use(bodyParser.urlencoded({ extended: true }));
        this.express.use(bodyParser.json());
        
        this.mountRoutes();
    }

    private async mountRoutes(): Promise<void> {
        const router = express.Router();

        router.get(
            '/ping',
            (req, res): void => {
                const result = { message: 'Pong' };
                res.json(result);
            },
        );

        this.express.use('/', router);
    }
}